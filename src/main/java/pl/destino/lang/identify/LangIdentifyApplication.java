package pl.destino.lang.identify;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@SpringBootApplication
@EnableWebMvc
public class LangIdentifyApplication {

	public static void main(String[] args) {
		SpringApplication.run(LangIdentifyApplication.class, args);
	}

}
