package pl.destino.lang.identify.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ManagedLanguage {

    BASQUE("Basque", "eus"), CATALAN("Catalan", "cat"), ARAGONESE("Aragonese", "arg"), INVALID("Invalid language. Accepted languages: Basque, Aragonese, Catalan.", null);

    private final String value;
    private final String languageCode;

    public static ManagedLanguage fromLanguageCode(String lang) {
	for (ManagedLanguage ml : ManagedLanguage.values()) {
	    if (lang.equals(ml.getLanguageCode())) {
		return ml;
	    }
	}
	return INVALID;
    }

}
