package pl.destino.lang.identify.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import opennlp.tools.langdetect.Language;
import opennlp.tools.langdetect.LanguageDetector;
import org.springframework.stereotype.Service;
import pl.destino.lang.identify.enums.ManagedLanguage;

@Slf4j
@Service
@RequiredArgsConstructor
public class LanguageIdentifierService {

    private final LanguageDetector languageDetectorModel;

    public ManagedLanguage detectLanguage(String sample) {
	Language[] language = languageDetectorModel.predictLanguages(sample);
	log.info("Detection results for sample sent:");
	for (Language l : language) {
	    log.info(" - Language {}, Confidence {}", l.getLang(), l.getConfidence());
	}
	return ManagedLanguage.fromLanguageCode(language[0].getLang());
    }

}
