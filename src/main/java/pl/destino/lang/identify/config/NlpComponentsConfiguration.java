package pl.destino.lang.identify.config;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import lombok.extern.slf4j.Slf4j;
import opennlp.tools.langdetect.LanguageDetector;
import opennlp.tools.langdetect.LanguageDetectorFactory;
import opennlp.tools.langdetect.LanguageDetectorME;
import opennlp.tools.langdetect.LanguageDetectorModel;
import opennlp.tools.langdetect.LanguageDetectorSampleStream;
import opennlp.tools.langdetect.LanguageSample;
import opennlp.tools.ml.perceptron.PerceptronTrainer;
import opennlp.tools.util.InputStreamFactory;
import opennlp.tools.util.MarkableFileInputStreamFactory;
import opennlp.tools.util.ObjectStream;
import opennlp.tools.util.PlainTextByLineStream;
import opennlp.tools.util.TrainingParameters;
import opennlp.tools.util.model.ModelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Slf4j
@Configuration
public class NlpComponentsConfiguration {

    @Bean(name = "languageDetectorModel")
    public LanguageDetectorModel languageDetectorModel() throws IOException {
	File modelFile = new File(getClass().getClassLoader().getResource("datamodel/langdetect.bin").getFile());
	log.info("Try to create model file.");
	if (!modelFile.exists()) {
	    log.info("Model file doesn't exist, creating...");
	    File trainingFile = new File(getClass().getClassLoader().getResource("texts/leipzig-1.txt").getFile());
	    InputStreamFactory inputStreamFactory = new MarkableFileInputStreamFactory(trainingFile);

	    ObjectStream<String> lineStream = new PlainTextByLineStream(inputStreamFactory, StandardCharsets.UTF_8);
	    ObjectStream<LanguageSample> sampleStream = new LanguageDetectorSampleStream(lineStream);

	    TrainingParameters params = ModelUtil.createDefaultTrainingParameters();
	    params.put(TrainingParameters.ALGORITHM_PARAM, PerceptronTrainer.PERCEPTRON_VALUE);
	    params.put(TrainingParameters.CUTOFF_PARAM, 0);

	    LanguageDetectorFactory factory = new LanguageDetectorFactory();

	    LanguageDetectorModel model = LanguageDetectorME.train(sampleStream, params, factory);
	    model.serialize(new File("langdetect.bin"));
	    log.info("Model file created. Creating bean.");
	    return model;
	} else {
	    log.info("Model file already exists. Creating bean.");
	    return new LanguageDetectorModel(modelFile);
	}
    }

    @Bean
    @DependsOn("languageDetectorModel")
    public LanguageDetector languageDector(@Autowired LanguageDetectorModel languageDetectorModel) {
	log.info("Create language detector bean.");
	return new LanguageDetectorME(languageDetectorModel);
    }

}
