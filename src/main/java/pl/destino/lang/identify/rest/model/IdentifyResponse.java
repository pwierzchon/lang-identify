package pl.destino.lang.identify.rest.model;

import java.io.Serializable;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Builder
@Data
public class IdentifyResponse implements Serializable {
    private final String language;
}
