package pl.destino.lang.identify.rest.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.destino.lang.identify.rest.model.IdentifyRequest;
import pl.destino.lang.identify.rest.model.IdentifyResponse;
import pl.destino.lang.identify.services.LanguageIdentifierService;

@RestController
@RequestMapping(("identification"))
@RequiredArgsConstructor
public class IdentifyLanguageController {
    
    private final LanguageIdentifierService languageIdentifierService;
    
    @PostMapping(value="/language",produces = MediaType.APPLICATION_JSON_VALUE,consumes=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<IdentifyResponse> language(@RequestBody  IdentifyRequest request){
	return ResponseEntity.ok(IdentifyResponse.builder().language(languageIdentifierService.detectLanguage(request.getText()).getValue()).build());
    }
    
}
