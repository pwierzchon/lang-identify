package pl.destino.lang.identify.rest.model;

import java.io.Serializable;
import lombok.Data;

@Data
public class IdentifyRequest implements Serializable {
    private String text;
}
